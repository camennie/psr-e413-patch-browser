/*
    PSR-E413_PatchBrowser -- A Linux based patch browser tool for the Yamaha PSR-E413
    Copyright (C) 2010 Chris A. Mennie

    This file is part of PSR-E413_PatchBrowser.

    PSR-E413_PatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PSR-E413_PatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PSR-E413_PatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "widgets.h"
#include "main.h"
#include <boost/lexical_cast.hpp>
#include "data/PSR-E413.h"
#include "Preferences.h"
#include <iostream>
#include <deque>
#include <stdlib.h>
#include <boost/function.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/thread.hpp>
#include <boost/foreach.hpp>
#include <cstdio>
#include <FLAC++/metadata.h>
#include <FLAC++/decoder.h>

namespace
{
    
FLAC__uint64 total_samples = 0;
unsigned sample_rate = 0;
unsigned channels = 0;
unsigned bps = 0;

class JackDecoder: public FLAC::Decoder::File 
{
    boost::shared_ptr<bounded_buffer<float> > playBuffer;
    bool onFirstDecode;
public:
	JackDecoder(): FLAC::Decoder::File() 
    { 
        JackSingleton &jack = JackSingleton::instance();
        playBuffer = jack.getPlayBuffer();
        onFirstDecode = true;
    }//constructor
protected:
	virtual ::FLAC__StreamDecoderWriteStatus write_callback(const ::FLAC__Frame *frame, const FLAC__int32 * const buffer[]);

    virtual void metadata_callback(const ::FLAC__StreamMetadata *metadata) 
    {
        /* print some stats */
        if(metadata->type == FLAC__METADATA_TYPE_STREAMINFO) {
            /* save for later */
            total_samples = metadata->data.stream_info.total_samples;
            sample_rate = metadata->data.stream_info.sample_rate;
            channels = metadata->data.stream_info.channels;
            bps = metadata->data.stream_info.bits_per_sample;

//            fprintf(stderr, "sample rate    : %u Hz\n", sample_rate);
//            fprintf(stderr, "channels       : %u\n", channels);
//            fprintf(stderr, "bits per sample: %u\n", bps);
//            fprintf(stderr, "total samples  : %llu\n", total_samples);
        }
//        std::cout << "metadata_callback" << std::endl; 
    }

	virtual void error_callback(::FLAC__StreamDecoderErrorStatus status) { std::cout << "error_callback" << std::endl; }
};//JackDecoder


bool stopRecordingAllPatches = true;
boost::shared_ptr<boost::function<void (void)> > recordAllThreadStartFunc;
boost::shared_ptr<boost::thread> recordAllThread;

bool stopPlayingSample = false;
boost::shared_ptr<boost::function<void (void)> > playSampleThreadStartFunc;
boost::shared_ptr<boost::thread> playSampleThread;
std::string g_sampleFilename;


    boost::array<const char *, 26> categories = {
        "--- / All",
        "PIANO",
        "E.PIANO", 
        "ORGAN", 
        "ACCORDION",
        "GUITAR",
        "BASS",
        "STRINGS",
        "CHOIR",
        "SAXOPHONE",
        "TRUMPET",
        "BRASS",
        "FLUTE",
        "SYNTH LEAD",
        "SYNTH PAD",
        "PERCUSSION",
        "DRUM KITS",
        "ARPEGGIO",
        "CHROMATIC",
        "ENSEMBLE",
        "REED",
        "PIPE",
        "SYNTH EFFECTS",
        "WORLD",
        "PERCUSSIVE",
        "SOUND EFFECTS"
    };//categories


std::string getCategoryStr(unsigned int category)
{
    if (category < categories.size()) {
        return categories[category];
    } else {
        return "UNKOWN";
    }//if
}//getCategoryStr

void changeSelection(boost::shared_ptr<PSR_E413Data::PatchInfo> curPatch, int curMidiChannel)
{
    std::vector<jack_midi_data_t> setMSB;
    std::vector<jack_midi_data_t> setLSB;
    std::vector<jack_midi_data_t> programChange;

    setMSB.push_back(0xb0 + curMidiChannel);
    setMSB.push_back(0x00);
    setMSB.push_back(curPatch->patchBankMSB);

    setLSB.push_back(0xb0 + curMidiChannel);
    setLSB.push_back(0x20);
    setLSB.push_back(curPatch->patchBankLSB);

    programChange.push_back(0xc0 + curMidiChannel);
    programChange.push_back(curPatch->patchNum);

    JackSingleton &jack = JackSingleton::instance();
    jack.queueMessage(&setMSB[0], setMSB.size());
    jack.queueMessage(&setLSB[0], setLSB.size());
    jack.queueMessage(&programChange[0], programChange.size());

    std::cout << "changeSelection" << std::endl;
}//changeSelection

::FLAC__StreamDecoderWriteStatus JackDecoder::write_callback(const ::FLAC__Frame *frame, const FLAC__int32 * const buffer[])
{
    //static int totalcnt = 0;

    float inv16Bit = 1.0f / ((float)0xffff);    

//    totalcnt += frame->header.blocksize;
//    std::cout << "write_callback: " << frame->header.blocksize << " - " << totalcnt << " - " << total_samples - totalcnt << std::endl;

    if (true == onFirstDecode) {
        onFirstDecode = false;
        playBuffer->clear();
        while (playBuffer->empty() == false);

        for (size_t i = 0; i < frame->header.blocksize; i++) {            
            float left = buffer[0][i] * inv16Bit;
            float right = buffer[1][i] * inv16Bit;

            playBuffer->push_front_no_count_update(left, right);
        }//for

        playBuffer->updateCount();
    } else {
        for (size_t i = 0; i < frame->header.blocksize; i++) {
            if (true == stopPlayingSample) {
                playBuffer->clear();
                return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
            }//if

            float left = buffer[0][i] * inv16Bit;
            float right = buffer[1][i] * inv16Bit;

            playBuffer->push_front(left, right);
        }//for
    }//if

	return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}//write_callback

void playSampleThreadFunc()
{
    stopPlayingSample = false;

//    std::cout << "in playSample" << std::endl;

    bool ok = true;
    JackDecoder decoder;
    
	FLAC__StreamDecoderInitStatus init_status = decoder.init(g_sampleFilename.c_str());
	if (init_status != FLAC__STREAM_DECODER_INIT_STATUS_OK) {
        std::cerr << "ERROR: initializing decoder: " << FLAC__StreamDecoderInitStatusString[init_status] << std::endl;
		ok = false;
	}//if

	if (ok) {
		ok = decoder.process_until_end_of_stream();
//        std::cerr << "decoding: " << (ok? "succeeded" : "FAILED") << std::endl;
//        std::cerr << "   state: " << decoder.get_state().resolved_as_cstring(decoder) << std::endl;
	}//if

//    std::cout << "Finished playing sample: " << g_sampleFilename << std::endl;
}//playSampleThreadFunc

void recordSample(std::string filename, Preferences *preferences)
{
    std::cout << "in recordSample" << std::endl;

    JackSingleton &jack = JackSingleton::instance();
    jack.setSampleFile(filename);

    std::cout << "Recording: " << filename << " with command: '" << preferences->getPlayMidiCommand() << "'" << std::endl;
    
    system(preferences->getPlayMidiCommand().c_str());

    std::cout << "Finished recording: " << filename << std::endl;
}//recordSample

std::string getSampleFilename(boost::shared_ptr<PSR_E413Data::PatchInfo> curPatch, Preferences *preferences)
{
    std::string patchName = curPatch->patchName;
    for (int pos = 0; pos < patchName.size(); ++pos) {
        if (patchName[pos] == '/') {
            patchName[pos] = '_';
        }//if
    }//for

    std::string patchNum = boost::lexical_cast<std::string>(curPatch->actualPatchNum);

    if (curPatch->actualPatchNum < 100) {
        patchNum = "0" + patchNum;
    }//if

    if (curPatch->actualPatchNum < 10) {
        patchNum = "0" + patchNum;
    }//if

    std::string filename =  patchNum + std::string("-") + patchName + std::string(".flac");
    filename = preferences->getSamplePath() + std::string("/") + filename;
    return filename;
}//getSampleFilename

void deleteFile(std::string filename)
{
    std::remove(filename.c_str());
}//deleteFile

void recordAllThreads(PSR_E413Librarian *librarian, Preferences *preferences, int curMidiChannel)
{
    JackSingleton &jack = JackSingleton::instance();
    boost::shared_ptr<PSR_E413Data::PSR_E413> psrE413 = librarian->getPSR_E413Data();
    std::vector<boost::shared_ptr<PSR_E413Data::PatchInfo> > currentPatchData = psrE413->getAllPatchInfos();
    BOOST_FOREACH (boost::shared_ptr<PSR_E413Data::PatchInfo> curPatch, currentPatchData) {
redo:        
        if (true == stopRecordingAllPatches) {
            break;
        }//if

        if (curPatch->category == 37) { //drums
            continue;
        }//if

        std::string patchFilename = getSampleFilename(curPatch, preferences);
        struct stat buffer;
        if (stat(patchFilename.c_str(), &buffer) != 0) {
            changeSelection(curPatch, curMidiChannel);
            boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

            recordSample(patchFilename, preferences);

            bool hasXRun = jack.setSampleFile("");
            if (true == hasXRun) {
                std::cout << "Had XRUN" << std::endl;
                deleteFile(patchFilename);
                goto redo;
            }//if

            std::cout << "Auto sample record finished recording: " << patchFilename << std::endl;
        }//if
    }//foreach

    std::cout << "Exiting recordAllThreads" << std::endl;
}//recordAllThreads

void checkForSamples(PSR_E413Librarian *librarian, Preferences *preferences)
{
    boost::shared_ptr<PSR_E413Data::PSR_E413> psrE413 = librarian->getPSR_E413Data();
    std::vector<boost::shared_ptr<PSR_E413Data::PatchInfo> > currentPatchData = psrE413->getAllPatchInfos();
    BOOST_FOREACH (boost::shared_ptr<PSR_E413Data::PatchInfo> curPatch, currentPatchData) {
        std::string patchFilename = getSampleFilename(curPatch, preferences);
        struct stat buffer;
        if (stat(patchFilename.c_str(), &buffer) != 0) {
            curPatch->hasSample = false;
        } else {
            curPatch->hasSample = true;
        }//if
    }//if
}//checkForSamples

}//anonymous namespace

PSR_E413Widgets::PSR_E413Widgets(PSR_E413Librarian *owner_)
{
//    currentMode = 0;
    owner = owner_;
    curMidiChannel = 0;
}//constructor

void PSR_E413Widgets::createWindow()
{
    PSR_E413Librarian *mainWindow = NULL;
    Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create("PSR-E413.glade");
    refXml->get_widget_derived("PatchWindow", mainWindow);
    assert(mainWindow != NULL);

    mainWindow->present();

    JackSingleton::instance().currentWindow = mainWindow;
}//createWindow

void PSR_E413Widgets::setUpWidgets(Glib::RefPtr<Gnome::Glade::Xml> refXml, Gtk::Window *window)
{
    preferences = new Preferences(refXml);

    activeWindows.push_back(this);

    refXml->get_widget("midiChannelSpinButton", midiChannelSpinButton);
    midiChannelSpinButton->signal_value_changed().connect(sigc::mem_fun(*this, &PSR_E413Widgets::handleMidiChannelChange));

    refXml->get_widget("MainTreeView", infoTreeView);

    refXml->get_widget("menu_open", menuOpen);
    refXml->get_widget("menu_save", menuSave);
    refXml->get_widget("menu_saveas", menuSaveAs);
    refXml->get_widget("menu_new", menuNew);
    refXml->get_widget("menu_quit", menuQuit);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Gtk::ListStore::create(infoColumns);
    this->infoTreeView->set_model(infoTreeViewListStore);
    
    infoTreeView->modify_base(Gtk::STATE_NORMAL, Gdk::Color("#999999")); 

//    infoTreeView->get_selection()->set_mode(Gtk::SELECTION_MULTIPLE);

    infoTreeView->append_column("ID", infoColumns.ID);
    infoTreeView->append_column("Name", infoColumns.Name);
    infoTreeView->append_column("Cat", infoColumns.Category);

    Gtk::TreeViewColumn *idColumn = infoTreeView->get_column(0);
    idColumn->set_resizable();
    idColumn->set_sort_indicator(true);
    idColumn->set_sort_column(infoColumns.ID);
    Gtk::TreeViewColumn *nameColumn = infoTreeView->get_column(1);
    nameColumn->set_resizable();
    Gtk::TreeViewColumn *catColumn = infoTreeView->get_column(2);
    catColumn->set_resizable();
    catColumn->set_sort_indicator(true);
    catColumn->set_sort_column(infoColumns.Category);

    Gtk::CellRenderer *renderer;
    
    renderer = idColumn->get_first_cell_renderer();
    idColumn->set_cell_data_func(*renderer, sigc::mem_fun(*this, &PSR_E413Widgets::formatColumn));
    renderer = nameColumn->get_first_cell_renderer();
    nameColumn->set_cell_data_func(*renderer, sigc::mem_fun(*this, &PSR_E413Widgets::formatColumn));
    renderer = catColumn->get_first_cell_renderer();
    catColumn->set_cell_data_func(*renderer, sigc::mem_fun(*this, &PSR_E413Widgets::formatColumn));

    infoTreeView->append_column_editable("Memo1", infoColumns.Memo1);
    infoTreeView->append_column_editable("Memo2", infoColumns.Memo2);
    infoTreeView->append_column_editable("Memo3", infoColumns.Memo3);
    infoTreeView->append_column_editable("Memo4", infoColumns.Memo4);
    infoTreeView->append_column("Sample", infoColumns.HasSample);

    Gtk::TreeViewColumn *memo1Column = infoTreeView->get_column(3);
    memo1Column->set_resizable();
    Gtk::CellRendererText *memo1Renderer = (Gtk::CellRendererText*)memo1Column->get_first_cell_renderer();
    memo1Renderer->signal_edited().connect(sigc::mem_fun(*this, &PSR_E413Widgets::memo1_on_edited));

    Gtk::TreeViewColumn *memo2Column = infoTreeView->get_column(4);
    memo2Column->set_resizable();
    Gtk::CellRendererText *memo2Renderer = (Gtk::CellRendererText*)memo2Column->get_first_cell_renderer();
    memo2Renderer->signal_edited().connect(sigc::mem_fun(*this, &PSR_E413Widgets::memo2_on_edited));

    Gtk::TreeViewColumn *memo3Column = infoTreeView->get_column(5);
    memo3Column->set_resizable();
    Gtk::CellRendererText *memo3Renderer = (Gtk::CellRendererText*)memo3Column->get_first_cell_renderer();
    memo3Renderer->signal_edited().connect(sigc::mem_fun(*this, &PSR_E413Widgets::memo3_on_edited));

    Gtk::TreeViewColumn *memo4Column = infoTreeView->get_column(6);
    memo4Column->set_resizable();
    Gtk::CellRendererText *memo4Renderer = (Gtk::CellRendererText*)memo4Column->get_first_cell_renderer();
    memo4Renderer->signal_edited().connect(sigc::mem_fun(*this, &PSR_E413Widgets::memo4_on_edited));

    Gtk::TreeViewColumn *hasSampleColumn = infoTreeView->get_column(7);
    //hasSampleColumn->set_resizable();
    Gtk::CellRendererText *hasSampleColumnRenderer = (Gtk::CellRendererText*)hasSampleColumn->get_first_cell_renderer();
    
    renderer = memo1Column->get_first_cell_renderer();
    memo1Column->set_cell_data_func(*renderer, sigc::mem_fun(*this, &PSR_E413Widgets::formatColumn));
    renderer = memo2Column->get_first_cell_renderer();
    memo2Column->set_cell_data_func(*renderer, sigc::mem_fun(*this, &PSR_E413Widgets::formatColumn));
    renderer = memo3Column->get_first_cell_renderer();
    memo3Column->set_cell_data_func(*renderer, sigc::mem_fun(*this, &PSR_E413Widgets::formatColumn));
    renderer = memo4Column->get_first_cell_renderer();
    memo4Column->set_cell_data_func(*renderer, sigc::mem_fun(*this, &PSR_E413Widgets::formatColumn));

    renderer = hasSampleColumn->get_first_cell_renderer();
    hasSampleColumn->set_cell_data_func(*renderer, sigc::mem_fun(*this, &PSR_E413Widgets::formatColumn));

    menuOpen->signal_activate().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_menuOpen));
    menuSave->signal_activate().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_menuSave));
    menuSaveAs->signal_activate().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_menuSaveAs));
    menuNew->signal_activate().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_menuNew));
    menuQuit->signal_activate().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_menuQuit));

    Gtk::MenuItem *menuItem;
    refXml->get_widget("recordSamplesMenuItem", menuItem);
    menuItem->signal_activate().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_menuRecordSamples));
    refXml->get_widget("stopRecordingMenuItem", menuItem);
    menuItem->signal_activate().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_menuStopRecording));

    Gtk::ImageMenuItem *menuItem2;
    refXml->get_widget("menu_prefs", menuItem2);
    menuItem->signal_activate().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_menuPrefs));

    window->signal_delete_event().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_windowClose));

    window->signal_key_press_event().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_key_pressed));
    infoTreeView->signal_key_press_event().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_key_pressed));
    infoTreeView->signal_cursor_changed().connect(sigc::mem_fun(*this, &PSR_E413Widgets::on_selectionChanged), false);
    
    refXml->get_widget("filterEntry", filterEntry);
    refXml->get_widget("categoryComboBox", categoryComboBox);
    
    Glib::RefPtr<Gtk::ListStore> comboListStore = Gtk::ListStore::create(categoryBoxColumns);
    categoryComboBox->set_model(comboListStore);
    categoryComboBox->pack_start(categoryBoxColumns.filename_col);
    
    std::set<std::string> categoriesSet;
    
    for (unsigned int pos = 0; pos < categories.size(); ++pos) {
	    categoriesSet.insert(categories[pos]);
    }//for
    
    for (std::set<std::string>::const_iterator setIter = categoriesSet.begin(); setIter != categoriesSet.end(); ++setIter) {
    	Gtk::TreeModel::Row row = *(comboListStore->append());
	    row[categoryBoxColumns.filename_col] = *setIter;
    }//for
    
    categoryComboBox->set_active(0);
            
    categoryComboBox->signal_changed().connect( sigc::mem_fun(*this, &PSR_E413Widgets::refreshUI) );
    filterEntry->signal_changed().connect( sigc::mem_fun(*this, &PSR_E413Widgets::refreshUI) );

    filterEntry->modify_base(Gtk::STATE_NORMAL, Gdk::Color("#dddddd"));
 
    hasNoSamplePng = Gdk::Pixbuf::create_from_file("./reload.png");
    hasSamplePng = Gdk::Pixbuf::create_from_file("./check.png");

    ///////////////////////////
    std::string filename = "CurrentSet-1.xml";
    boost::shared_ptr<PSR_E413Data::PSR_E413> baseData = owner->getPSR_E413Data();
    baseData->loadFromFile(filename);
    curFilename = filename;
    checkForSamples(owner, preferences);

    refreshUI();
}//setUpWidgets

void PSR_E413Widgets::handleMidiChannelChange()
{
    curMidiChannel = midiChannelSpinButton->get_value_as_int();
}//handleMidiChannelChange

void PSR_E413Widgets::doPlaySample(bool forceRecord)
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
//    unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<PSR_E413Data::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];

    std::string filename = getSampleFilename(curPatch, preferences);

    struct stat buffer;
    if ((false == forceRecord) && (stat(filename.c_str(), &buffer) == 0)) {
        curRow[infoColumns.HasSample] = true;
//        ... play sample
    } else {
        JackSingleton &jack = JackSingleton::instance();

        recordSample(filename, preferences);

        bool hasXRun = jack.setSampleFile("");
        if (true == hasXRun) {
            std::cout << "Had XRUN" << std::endl;
            deleteFile(filename);
        }//if
        curRow[infoColumns.HasSample] = true;
    }//if

    std::cout << "'" << filename << "'" << std::endl;
}//doPlaySample

void PSR_E413Widgets::on_menuRecordSamples()
{
    std::cout << "on_menuRecordSamples" << std::endl;

    if (recordAllThread == NULL) {
        stopRecordingAllPatches = false;
        recordAllThreadStartFunc.reset(new boost::function<void (void)>(boost::lambda::bind(&recordAllThreads, boost::lambda::var(owner), boost::lambda::var(preferences), boost::lambda::var(curMidiChannel))));
        recordAllThread.reset(new boost::thread(*recordAllThreadStartFunc));
    }//if
}//on_menuRecordSamples

void PSR_E413Widgets::on_menuStopRecording()
{
    if (stopRecordingAllPatches == false) {
        std::cout << "Stopping after next patch" << std::endl;
    }//if
    stopRecordingAllPatches = true;

    if (recordAllThread != NULL) {
        recordAllThread->join();
        checkForSamples(owner, preferences);
        refreshUI();
    }//if
}//on_menuStopRecording

bool PSR_E413Widgets::on_key_pressed(GdkEventKey *event)
{
    char key = *(event->string);

    if (key == 's') {
        JackSingleton &jack = JackSingleton::instance();
        jack.getPlayBuffer()->clear();
        stopPlayingSample = true;
        if (playSampleThread != NULL) {
            playSampleThread->join();
            playSampleThread.reset();
        }//if
        doPlaySample(false);
    }//if

    return true;
}//on_key_pressed

bool PSR_E413Widgets::on_windowClose(GdkEventAny *)
{
    std::vector<PSR_E413Widgets *>::iterator windowIter = std::find(activeWindows.begin(), activeWindows.end(), this);

    if (windowIter != activeWindows.end()) {
        activeWindows.erase(windowIter);
    }//if

    if (true == activeWindows.empty()) {
        Gtk::Main::quit();
    }//if

    return false;
}//on_windowClose

void PSR_E413Widgets::on_selectionChanged()
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
//    unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<PSR_E413Data::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];
    changeSelection(curPatch, curMidiChannel);

//    std::cout << "Selected: " << curPatch->patchName << std::endl;
}//on_selectionChanged

void PSR_E413Widgets::on_menuQuit()
{
    Gtk::Main::quit();
}//on_menuQuit

void PSR_E413Widgets::on_menuPrefs()
{
    preferences->show();
}//on_menuPrefs

void PSR_E413Widgets::on_menuNew()
{
    PSR_E413Widgets::createWindow();
}//on_menuNew

void PSR_E413Widgets::on_menuSave()
{
    if (false == curFilename.empty()) {
        boost::shared_ptr<PSR_E413Data::PSR_E413> baseData = owner->getPSR_E413Data();
        baseData->saveToFile(curFilename);

        for (unsigned int index = 0; index < 256; ++index) {
//            baseData->setPatchDirtyFlag(index, false);
        }//for
    } else {
        on_menuSaveAs();
    }//if

    refreshUI();
}//on_menuSave

void PSR_E413Widgets::on_menuSaveAs()
{
    Gtk::FileChooserDialog dialog("Save...", Gtk::FILE_CHOOSER_ACTION_SAVE);
//    dialog.set_transient_for(*this);
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);

    Gtk::FileFilter filter_any;
    filter_any.set_name("Any files");
    filter_any.add_pattern("*");
    dialog.add_filter(filter_any);

    int result = dialog.run();
    switch(result) {
        case(Gtk::RESPONSE_OK):
        {
            std::string filename = dialog.get_filename();
            boost::shared_ptr<PSR_E413Data::PSR_E413> baseData = owner->getPSR_E413Data();
            baseData->saveToFile(filename);
            curFilename = filename;

            break;
        }
        case(Gtk::RESPONSE_CANCEL):
            break;
        default:
            break;
    }//switch

    refreshUI();
}//on_menuSave

void PSR_E413Widgets::on_menuOpen()
{
     Gtk::FileChooserDialog dialog("Load", Gtk::FILE_CHOOSER_ACTION_OPEN);
//    dialog.set_transient_for(*this);
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);

    Gtk::FileFilter filter_any;
    filter_any.set_name("Any files");
    filter_any.add_pattern("*");
    dialog.add_filter(filter_any);

    int result = dialog.run();
    switch(result) {
        case(Gtk::RESPONSE_OK):
        {
            std::string filename = dialog.get_filename();
            boost::shared_ptr<PSR_E413Data::PSR_E413> baseData = owner->getPSR_E413Data();
            baseData->loadFromFile(filename);
            curFilename = filename;
            checkForSamples(owner, preferences);

            break;
        }
        case(Gtk::RESPONSE_CANCEL):
            break;
        default:
            break;
    }//switch 

    refreshUI();    
}//on_menuOpen

void PSR_E413Widgets::memo1_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text)
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
 //   unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<PSR_E413Data::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];

//std::cout << "Set memo to " << new_text << " from " << curPatch->memo1 << std::endl;

    curPatch->memo1 = new_text;

//    refreshUI();
}//memo1_on_edited

void PSR_E413Widgets::memo2_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text)
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
//    unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<PSR_E413Data::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];

//std::cout << "Set memo to " << new_text << " from " << curPatch->memo1 << std::endl;

    curPatch->memo2 = new_text;

//    refreshUI();
}//memo2_on_edited

void PSR_E413Widgets::memo3_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text)
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
  //  unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<PSR_E413Data::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];

//std::cout << "Set memo to " << new_text << " from " << curPatch->memo1 << std::endl;

    curPatch->memo3 = new_text;

//    refreshUI();
}//memo3_on_edited

void PSR_E413Widgets::memo4_on_edited(const Glib::ustring &path_string, const Glib::ustring &new_text)
{
    Glib::RefPtr<Gtk::TreeView::Selection> selection = infoTreeView->get_selection();
    Glib::ListHandle<Gtk::TreePath, Gtk::TreePath_Traits> rows = selection->get_selected_rows();

    Glib::Container_Helpers::ListHandleIterator<Gtk::TreePath_Traits> iter = rows.begin();

    Gtk::TreePath path = *iter;
    Glib::ustring pathStr = path.to_string();
    //unsigned int row = boost::lexical_cast<unsigned int>(pathStr);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());

    Gtk::TreeModel::Row curRow = *(infoTreeViewListStore->get_iter(path));

    boost::shared_ptr<PSR_E413Data::PatchInfo> curPatch = curRow[infoColumns.PatchInfoCopy];

//std::cout << "Set memo to " << new_text << " from " << curPatch->memo1 << std::endl;

    curPatch->memo4 = new_text;

//    refreshUI();
}//memo4_on_edited

void PSR_E413Widgets::refreshUI()
{
    Glib::ustring curCategory;
    std::string filterText;
    
    filterText = filterEntry->get_text();
    transform(filterText.begin(), filterText.end(), filterText.begin(), tolower);
    
    Gtk::TreeModel::const_iterator categoryIter = categoryComboBox->get_active();
    Gtk::TreeModel::Row row = *categoryIter;
    curCategory = row[categoryBoxColumns.filename_col];
    
    boost::shared_ptr<PSR_E413Data::PSR_E413> baseData = owner->getPSR_E413Data();
    std::vector<boost::shared_ptr<PSR_E413Data::PatchInfo> > patches = baseData->getAllPatchInfos();

    std::string title = std::string("PSR-E413 Patch Browser - ") + curFilename;

    bool isDirty = false;

    if (true == isDirty) {
        title = title + std::string(" *");
    }//if

    owner->set_title(title);

    boost::recursive_mutex::scoped_lock lock(owner->widgetsMutex);

//    modeBox->set_active(currentMode);

    Glib::RefPtr<Gtk::ListStore> infoTreeViewListStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(infoTreeView->get_model());
    infoTreeViewListStore->clear();

    for (unsigned int pos = 0; pos < patches.size(); ++pos) {
        std::string categoryStr = getCategoryStr(patches[pos]->category);
        
        if ((curCategory != categories[0]) && (curCategory != categoryStr)) {
            continue;
        }//if
               
        std::string patchName = patches[pos]->patchName;
        transform(patchName.begin(), patchName.end(), patchName.begin(), tolower);
        if ((filterText.length() > 0) && (patchName.find(filterText) == std::string::npos)) {
            continue;
        }//if
	
        Gtk::TreeModel::Row newRow = *(infoTreeViewListStore->append());
        newRow[infoColumns.ID] = pos + 1;
        
        newRow[infoColumns.Category] = categoryStr;

        newRow[infoColumns.Name] = patches[pos]->patchName;

        newRow[infoColumns.ID] = patches[pos]->actualPatchNum;

        newRow[infoColumns.PatchInfoCopy] = patches[pos];

        newRow[infoColumns.Memo1] = patches[pos]->memo1;
        newRow[infoColumns.Memo2] = patches[pos]->memo2;
        newRow[infoColumns.Memo3] = patches[pos]->memo3;
        newRow[infoColumns.Memo4] = patches[pos]->memo4;

        newRow[infoColumns.HasSample] = patches[pos]->hasSample;
    }//for
}//refreshUI

void PSR_E413Widgets::formatColumn(Gtk::CellRenderer *r, const Gtk::TreeModel::iterator& it)
{
    Glib::RefPtr<const Gtk::TreeModel> treeModel = infoTreeView->get_model();
    Gtk::TreeModel::Path path = treeModel->get_path(it);
    int row = path[0];
    
    if (row % 2) {
	r->property_cell_background() = "#aaaaaa";
    } else {
	r->property_cell_background() = "#dddddd";
    }//if
}//formatColumn

