/*
    PSR-E413_PatchBrowser -- A Linux based patch browser tool for the Yamaha PSR-E413
    Copyright (C) 2010 Chris A. Mennie

    This file is part of PSR-E413_PatchBrowser.

    PSR-E413_PatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PSR-E413_PatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PSR-E413_PatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <boost/shared_ptr.hpp>
#include <jack/jack.h>
#include <iostream>
#include <libglademm/xml.h>
#include <gtkmm.h>
#include "main.h"
#include "widgets.h"
#include <sigc++/sigc++.h>

std::vector<PSR_E413Widgets *> activeWindows;

JackSingleton *JackSingleton::JSInstance = NULL;

namespace 
{


}//anonymous namespace

int main(int argc, char **argv)
{
    Gtk::Main gtkMain(argc, argv);

    JackSingleton::instance().initJack();

    if (NULL == JackSingleton::instance().getJackClient()) {
        std::cerr << "Couldn't create JACK ports. Exiting." << std::endl;
    }//if

    PSR_E413Widgets::createWindow();

    boost::shared_ptr<PSR_E413Data::PSR_E413> baseData = JackSingleton::instance().currentWindow->getPSR_E413Data();

    Gtk::Main::run();

    return 0;
}//main

JackSingleton::JackSingleton()
{
    jackClient = NULL;
}//constructor

JackSingleton::~JackSingleton()
{
    jack_client_close(jackClient);
}//destrcutor

PSR_E413Librarian::PSR_E413Librarian(BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& refXml) : Gtk::Window(cobject)
{
    psr_E413Data.reset(new PSR_E413Data::PSR_E413);
    widgets.reset(new PSR_E413Widgets(this));

//    psr_E413Data->loadBootstrap();

    widgets->setUpWidgets(refXml, this);
}//constructor

PSR_E413Librarian::~PSR_E413Librarian()
{
    //Nothing
}//destructor

JackSingleton &JackSingleton::instance()
{
    if (NULL == JackSingleton::JSInstance) {
        JackSingleton::JSInstance = new JackSingleton;
    }//if

    return *JackSingleton::JSInstance;
}//instance

boost::shared_ptr<PSR_E413Data::PSR_E413> PSR_E413Librarian::getPSR_E413Data() const
{
    return psr_E413Data;
}//getSonicCellData


