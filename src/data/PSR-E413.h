/*
    PSR-E413_PatchBrowser -- A Linux based patch browser tool for the Yamaha PSR-E413
    Copyright (C) 2010 Chris A. Mennie

    This file is part of PSR-E413_PatchBrowser.

    PSR-E413_PatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PSR-E413_PatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PSR-E413_PatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __PSR_E413_H
#define __PSR_E413_H

#include <boost/shared_ptr.hpp>
#include <boost/array.hpp>
#include <vector>
#include <map>
#include <deque>
#include <set>
#include <jack/midiport.h>
#include <boost/serialization/version.hpp>

class PSR_E413Widgets;

namespace PSR_E413Data
{

struct PatchInfo
{
    std::string patchName;

    unsigned char patchBankMSB;
    unsigned char patchBankLSB;
    unsigned char patchNum;
    unsigned int actualPatchNum;
    unsigned char category;

    std::string memo1;
    std::string memo2;
    std::string memo3;
    std::string memo4;

    bool hasSample;

    PatchInfo()
    {
        hasSample = false;
    }//constructor
};//PatchInfo

class PSR_E413
{
    std::map<int, boost::shared_ptr<PatchInfo> > PatchInfos;

public:
    PSR_E413();
    virtual ~PSR_E413();

    std::vector<boost::shared_ptr<PatchInfo> > getAllPatchInfos() const;

    void loadBootstrap();

    void loadFromFile(const std::string &filename);
    void saveToFile(const std::string &filename);

    friend class ::PSR_E413Widgets;
};//PSR_E413

}//namespace PSR_E413Data

BOOST_CLASS_VERSION(PSR_E413Data::PSR_E413, 1);

#endif

