/*
    PSR-E413_PatchBrowser -- A Linux based patch browser tool for the Yamaha PSR-E413
    Copyright (C) 2010 Chris A. Mennie

    This file is part of PSR-E413_PatchBrowser.

    PSR-E413_PatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PSR-E413_PatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PSR-E413_PatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "PSR-E413.h"
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/access.hpp>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <iostream>
#include <vector>
#include <list>
#include <libxml++/libxml++.h>
#include "../main.h"

namespace {
    boost::array<const char *, 26> categories = {
        "--- / All",
        "PIANO",
        "E.PIANO", 
        "ORGAN", 
        "ACCORDION",
        "GUITAR",
        "BASS",
        "STRINGS",
        "CHOIR",
        "SAXOPHONE",
        "TRUMPET",
        "BRASS",
        "FLUTE",
        "SYNTH LEAD",
        "SYNTH PAD",
        "PERCUSSION",
        "DRUM KITS",
        "ARPEGGIO",
        "CHROMATIC",
        "ENSEMBLE",
        "REED",
        "PIPE",
        "SYNTH EFFECTS",
        "WORLD",
        "PERCUSSIVE",
        "SOUND EFFECTS"
    };//categories

int findCategoryIndex(std::string categoryStr)
{
    for (int pos = 0; pos < categories.size(); ++pos) {
        if(categoryStr == std::string(categories[pos])) {
            return pos;
        }//if
    }//for

    return 0;
}//findCategoryIndex

}//anonymous namespace

PSR_E413Data::PSR_E413::PSR_E413()
{
    //Nothing
}//constructor

PSR_E413Data::PSR_E413::~PSR_E413()
{
    //Nothing
}//destructor

void PSR_E413Data::PSR_E413::loadFromFile(const std::string &filename)
{
	std::ifstream inputStream(filename.c_str());
	if (false == inputStream.good()) {
		return;
	}//if

	boost::archive::xml_iarchive inputArchive(inputStream);

    unsigned int numPatches = PatchInfos.size();
    inputArchive & BOOST_SERIALIZATION_NVP(numPatches);

    PatchInfos.clear();
    for (unsigned int index = 0; index < numPatches; ++index) {
        PSR_E413Data::PatchInfo patchInfo;

        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchName);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchBankMSB);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchBankLSB);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchNum);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.actualPatchNum);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.category);

        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo1);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo2);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo3);
        inputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo4);

        boost::shared_ptr<PatchInfo> newPatchInfo(new PatchInfo);
        *newPatchInfo = patchInfo;
        PatchInfos.insert(std::make_pair(patchInfo.actualPatchNum, newPatchInfo));

        //For rosegarden device definition files
//        int patchNum = patchInfo.patchNum + 1;
//        std::cout << patchInfo.bankName << " , " << (unsigned int)patchInfo.patchBankMSB << " , "
//                  << (unsigned int)patchInfo.patchBankLSB << " , " << patchNum << " , " 
//                  << patchInfo.patchName << std::endl;

    }//for

    inputStream.close();
}//loadFromFile

void PSR_E413Data::PSR_E413::saveToFile(const std::string &filename)
{
	std::ofstream outputStream(filename.c_str());
	if (false == outputStream.good()) {
		return;
	}//if

	boost::archive::xml_oarchive outputArchive(outputStream);

    unsigned int numPatches = PatchInfos.size();
    outputArchive & BOOST_SERIALIZATION_NVP(numPatches);
    for (std::map<int, boost::shared_ptr<PatchInfo> >::const_iterator patchIter = PatchInfos.begin(); patchIter != PatchInfos.end(); ++patchIter) {
        PSR_E413Data::PatchInfo patchInfo = *patchIter->second;

        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchName);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchBankMSB);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchBankLSB);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.patchNum);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.actualPatchNum);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.category);

        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo1);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo2);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo3);
        outputArchive & BOOST_SERIALIZATION_NVP(patchInfo.memo4);
    }//for

    outputStream.close();
}//saveToFile

void PSR_E413Data::PSR_E413::loadBootstrap()
{
    xmlpp::DomParser parser;
    parser.set_substitute_entities(); //We just want the text to be resolved/unescaped automatically.
    parser.parse_file("voices.xml");
    if(parser) {
        //Walk the tree:
        const xmlpp::Node* rootNode = parser.get_document()->get_root_node(); //deleted by DomParser.

        xmlpp::Node::NodeList list = rootNode->get_children();
        for(xmlpp::Node::NodeList::iterator iter = list.begin(); iter != list.end(); ++iter) {
            xmlpp::Node *voiceNode = *iter;

            boost::shared_ptr<PatchInfo> curPatch(new PatchInfo);

            const xmlpp::Element* nodeElement = dynamic_cast<const xmlpp::Element*>(voiceNode);
            if (nodeElement == NULL) {
                continue;
            }//if

            const xmlpp::Attribute* attribute = nodeElement->get_attribute("id");
            curPatch->actualPatchNum = boost::lexical_cast<int>(attribute->get_value());

            xmlpp::Node::NodeList voiceChildList = voiceNode->get_children();
            for(xmlpp::Node::NodeList::iterator iter = voiceChildList.begin(); iter != voiceChildList.end(); ++iter) {
                xmlpp::Node *voiceChildNodeBase = *iter;
                const xmlpp::Element* voiceChildNode = dynamic_cast<const xmlpp::Element*>(voiceChildNodeBase);

                if (std::string(voiceChildNode->get_name()) == std::string("category")) {
                    xmlpp::TextNode *textChild = dynamic_cast<xmlpp::TextNode*>(*voiceChildNode->get_children().begin());
                    std::string categoryStr = textChild->get_content();
                    curPatch->category = findCategoryIndex(categoryStr);
                }//if

                if (std::string(voiceChildNode->get_name()) == std::string("msb")) {
                    xmlpp::TextNode *textChild = dynamic_cast<xmlpp::TextNode*>(*voiceChildNode->get_children().begin());
                    std::string msbStr = textChild->get_content();
                    curPatch->patchBankMSB = boost::lexical_cast<int>(msbStr);
                }//if

                if (std::string(voiceChildNode->get_name()) == std::string("lsb")) {
                    xmlpp::TextNode *textChild = dynamic_cast<xmlpp::TextNode*>(*voiceChildNode->get_children().begin());
                    std::string lsbStr = textChild->get_content();
                    curPatch->patchBankLSB = boost::lexical_cast<int>(lsbStr);
                }//if

                if (std::string(voiceChildNode->get_name()) == std::string("pc")) {
                    xmlpp::TextNode *textChild = dynamic_cast<xmlpp::TextNode*>(*voiceChildNode->get_children().begin());
                    std::string pcStr = textChild->get_content();
                    curPatch->patchNum = boost::lexical_cast<int>(pcStr);
                }//if

                if (std::string(voiceChildNode->get_name()) == std::string("name")) {
                    xmlpp::TextNode *textChild = dynamic_cast<xmlpp::TextNode*>(*voiceChildNode->get_children().begin());
                    std::string nameStr = textChild->get_content();
                    curPatch->patchName = nameStr;
                }//if
            }//for

            //std::cout << "Processed: " << curPatch->actualPatchNum << " - " << curPatch->patchName << std::endl;
            PatchInfos[curPatch->actualPatchNum] = curPatch;
        }//for
    }//if
}//loadBootstrap

std::vector<boost::shared_ptr<PSR_E413Data::PatchInfo> > PSR_E413Data::PSR_E413::getAllPatchInfos() const
{
    std::vector<boost::shared_ptr<PatchInfo> > retValues;

    for (std::map<int, boost::shared_ptr<PatchInfo> >::const_iterator patchIter = PatchInfos.begin(); patchIter != PatchInfos.end(); ++patchIter) {
        retValues.push_back(patchIter->second);
    }//for

    return retValues;
}//getAllPatchInfos


