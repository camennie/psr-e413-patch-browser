/*
    PSR-E413_PatchBrowser -- A Linux based patch browser tool for the Yamaha PSR-E413
    Copyright (C) 2010 Chris A. Mennie

    This file is part of PSR-E413_PatchBrowser.

    PSR-E413_PatchBrowser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PSR-E413_PatchBrowser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PSR-E413_PatchBrowser.  If not, see <http://www.gnu.org/licenses/>.
*/


