# Makefile

SonicCellPatchBrowser:
	cd src && make
	cp src/SonicCellPatchBrowser .

clean:
	rm -f SonicCellPatchBrowser
	cd src && make clean

PHONY: clean

